import React from 'react'
import { NavLink } from 'react-router-dom'

import './NavLinks.css'

const NavLinks = props => {
    return <ul className="nav-links">
        <li>
            <NavLink to="/" exact>ALL POSTS</NavLink>
        </li>
        <li>
            <NavLink to="/u1/posts">MY POSTS</NavLink>
        </li>
        <li>
            <NavLink to="/posts/new">ADD POST</NavLink>
        </li>
        <li>
            <NavLink to="/auth">AUTHENTICATION</NavLink>
        </li>
    </ul>
}

export default NavLinks