import logo from './logo.svg';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';

import Users from './user/pages/Users' ;
import Posts from './posts/pages/Posts' ;
import NewPlace from './posts/pages/NewPost';
import MainNavigation from './shared/components/Navigation/MainNavigation'


import './App.css';

const App = () => {
  return (
    <Router>
      <MainNavigation />
      <main>
        <Switch>
          <Route path="/" exact={true}>
            <Posts />
          </Route>
          <Route path="/posts/new" exact={true}>
            <NewPlace />
          </Route>
          <Redirect to="/" />
        </Switch>
      </main>
    </Router>
  );
}

export default App;
