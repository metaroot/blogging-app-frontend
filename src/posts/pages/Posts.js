import React from 'react';

import PostsList from '../components/PostsList'

const Posts = () => {
    const POSTS = [
        {id: 'u1', title: 'Good Vibe1', status: 'active'},
        {id: 'u2', title: 'Good Vibe2', status: 'active'},
        {id: 'u3', title: 'Good Vibe3', status: 'active'},
        {id: 'u4', title: 'Good Vibe4', status: 'active'},
        {id: 'u5', title: 'Good Vibe5', status: 'active'},
        {id: 'u6', title: 'Good Vibe6', status: 'active'},
        {id: 'u7', title: 'Good Vibe7', status: 'active'},
        {id: 'u8', title: 'Good Vibe8', status: 'active'},
        {id: 'u9', title: 'Good Vibe9', status: 'active'},
        {id: 'u10', title: 'Good Vibe10', status: 'active'},
        {id: 'u11', title: 'Good Vibe11', status: 'active'},
        {id: 'u12', title: 'Good Vibe12', status: 'active'},
    ];
    return <PostsList items={POSTS}/>;
};

export default Posts;