import React from 'react'
import { Link } from 'react-router-dom'

import Card from '../../shared/components/UIElements/Card'

import './PostCard.css'

const PostCard = props => {
    return (
        <li className="post-item">
            <Card className="post-item__content">
                <Link to={`/${props.id}/details`}>
                    <div className="post-item__info">
                        <h2>{props.title}</h2>
                        <h3>{props.status}</h3>
                    </div>
                </Link>
            </Card>
        </li>
    );
};

export default PostCard;