import React from 'react'
import './PostsList.css'

import PostCard from './PostCard'
import Card from '../../shared/components/UIElements/Card'

const PostsList = props => {
    if(props.items.length === 0) {
        return <div className="center">
            <Card>
                <h2>No Posts found.</h2>
            </Card>
        </div>
    } 
    return (
        <ul className="posts-list">
            {props.items.map(post => (
                <PostCard 
                key={post.id} 
                id={post.id} 
                title={post.title} 
                status={post.status} />
            ))}
        </ul>
    );
};

export default PostsList;